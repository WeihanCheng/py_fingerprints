import numpy as np
from scipy.fftpack import fftn
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy.matlib
import cv2
import math
import os
from mpl_toolkits.mplot3d import axes3d
import buildFilter
from skimage.measure import compare_ssim as ssim

from matplotlib.patches import Circle

paddingSize = 150

def analysis(bitNumber, HPF_Th=0.0001, LPFDistStep=23, d0=1.0, imgName='without glass[1].png', outFile='./deblurredImgBPF', BPFradius=90):

    ### read image
    img = cv2.imread(imgName)
    if not img.shape[0]/2 == 0:
        img = cv2.copyMakeBorder(img, 1, 0, 0, 0, cv2.BORDER_CONSTANT, value=[255, 255, 255])
    if not img.shape[1] / 2 == 0:
        img = cv2.copyMakeBorder(img, 0, 0, 1, 0, cv2.BORDER_CONSTANT, value=[255, 255, 255])

    #plt.imshow(img, cmap='gray'), plt.title('img'), plt.show()

    img = img[:, :, 1]

    distStep = LPFDistStep
    LPF = buildFilter.LPF(distStep, d0)

    ### pad img for more fft resolution
    padImg = cv2.copyMakeBorder(img, paddingSize, paddingSize, paddingSize, paddingSize, cv2.BORDER_CONSTANT, value=[255, 255, 255])

    #plt.imshow(padImg, cmap='gray'), plt.title('img'), plt.show()
    rows, cols = padImg.shape
    padImg = padImg.astype(np.float64)

    ### convolution to get blurred image
    blurredPadImg = cv2.filter2D(padImg, -1, LPF)

    """
    Quantization
    """
    blurredPadImg = (np.round((blurredPadImg/255.) * (2**bitNumber - 1)) / (2**bitNumber - 1)) * 255.

    #plt.imshow(blurredPadImg, cmap='gray'), plt.title('blurred image in spatial domain'), plt.show()
    #plt.imsave('test.png', blurredPadImg, cmap=cm.gray)

    ### convert to frequency domain
    fftBlurredPadImg = np.fft.fft2(blurredPadImg)
    fftBlurredPadImg = np.fft.fftshift(fftBlurredPadImg)
    #plt.imshow(np.log10(abs(fftBlurredPadImg)), cmap='gray'), plt.title('blurred image in frequency domain'), plt.show()



    fftHPF = buildFilter.fftHPF(rows, cols, 23, HPF_Th)
    padLPFrows = np.int((rows - LPF.shape[0]) / 2)
    padLPFcols = np.int((cols - LPF.shape[1]) / 2)
    padLPF = cv2.copyMakeBorder(LPF, padLPFrows, padLPFrows, padLPFcols, padLPFcols, cv2.BORDER_CONSTANT, value=0)
    fftpadLPF = np.fft.fft2(padLPF)
    fftpadLPF = np.fft.fftshift(fftpadLPF)


    ############## BPF 20dB decay
    radius = BPFradius
    fftBPF = fftHPF
    centerX = (cols - 1) / 2
    centerY = (rows - 1) / 2

    for i in range(0, rows):
        for j in range(0, cols):
            x = abs(j - centerX)
            y = abs(i - centerY)
            distance = np.sqrt(x ** 2 + y ** 2)
            if (distance > radius):
                power = (distance - radius) if (distance - radius) < 20 else 20
                fftBPF[i, j] = fftBPF[i, j] / (10 ** power)

    #plt.imshow(20 * np.log10(abs(fftBPF) + 0.1), cmap='gray'), plt.title('fftBPF'), plt.show()
    fftHPF = fftBPF
    ##############

    """
    ### ideal BPF
    fftBPF = np.zeros((rows, cols))
    cv2.circle(fftBPF, (int((cols - 1) / 2), int((rows - 1) / 2)), 90, 1, -1)
    fftBPF = fftBPF.astype(np.complex128)
    fftHPF = fftHPF * fftBPF
    """

    #Z1 = fftpadLPF[int(-(rows - 1) / 2), :]
    #plt.plot(20 * np.log10(abs(Z1)), color='blue', label='LPF'), plt.xlim(0, cols), plt.title('1D fftpadLPF')
    #Z2 = fftHPF[int(-(rows - 1) / 2), :]
    #plt.plot(20*np.log10(abs(Z2)+0.000000001), color='green', label='HPF'), plt.xlim(0, cols), plt.title('1D fftHPF')
    #plt.savefig('./waveFigBPF/'+'dist=' + str(LPFDistStep) + '_'+'th=' + str(HPF_Th) + '_d0='+str(d0) + '.png', dpi=150)
    #plt.close()

    #BPF = np.zeros((rows, cols))
    #cv2.circle(BPF, (int((cols-1)/2), int((rows-1)/2)), 90, 1, -1)
    #cv2.circle(BPF, (int((cols - 1) / 2), int((rows - 1) / 2)), 40, 0, -1)
    #plt.imshow(BPF, cmap='gray'), plt.title('fftPadImg'), plt.show()

    fftPadImg = np.fft.fft2(padImg)
    fftPadImg = np.fft.fftshift(fftPadImg)
    #plt.imshow(20 * np.log10(abs(fftPadImg) + 0.1), cmap='gray'), plt.title('fftPadImg'), plt.show()
    #fftPadImg = fftPadImg * BPF

    #restoPadImg = np.fft.ifftshift(fftPadImg)
    #restoPadImg = np.fft.ifft2(restoPadImg)
    #plt.imshow(np.real(restoPadImg), cmap='gray'), plt.title('fftPadImg'), plt.show()
    """
    Z1D = fftHPF[int(-(rows - 1) / 2), :]
    for i in range(337, cols):
        Z1D[i] = Z1D[i] / (10**(i-336))

    plt.plot(20*np.log10(abs(Z1D))), plt.xlim(0, cols), plt.title('1D abs(fftHPF)')
    plt.show()
    """

    #plt.imshow(20*np.log10(abs(fftHPF)+0.1), cmap='gray'), plt.title('high pass filter in frequency domain'), plt.show()



    ### restore image by HPF
    restoredImg = fftBlurredPadImg * fftHPF
    restoredImg = np.fft.ifftshift(restoredImg)
    restoredImg = np.fft.ifft2(restoredImg)
    restoredImg = np.fft.ifftshift(restoredImg)
    restoredImg = np.real(restoredImg)

    restoredImg = restoredImg[paddingSize: rows - paddingSize, paddingSize: cols - paddingSize]
    padImg = padImg[paddingSize: rows - paddingSize, paddingSize: cols - paddingSize]

    ssimResult = ssim(padImg, restoredImg)

    outName = imgName.split('.')[0]
    outName = outName.split('/')[1]

    imgOutName = outFile + '/' + outName + '_' \
              + 'bitN=' + str(bitNumber) \
              +'_dist=' + str(LPFDistStep) \
              + '_th=' + str(HPF_Th) + '_d0=' + str(d0) + '.png'

    #plt.imsave('OriImage.png', padImg, cmap=cm.gray)
    plt.imsave(imgOutName, restoredImg, cmap=cm.gray)
    #plt.imshow(restoredImg, cmap='gray'), plt.title('restoredImg'), plt.show()


    return ssimResult

def main():

    bitNumMin = 8
    bitNumMax = 16

    fileName = ['DB1_B', 'DB2_B']


    for file in fileName:
        outFile = file + '_out'
        if not os.path.exists(outFile):
            os.makedirs(outFile)
        for batchImg in os.listdir(file):
            print(batchImg)
            for bitNumber in range(bitNumMin, bitNumMax + 1):
                for d0 in np.arange(0.8, 1.3, 0.1):
                    if file == 'DB1_B':
                        radius = 120
                    else:
                        radius = 80
                    analysis(bitNumber, 0.0008, 23, d0, file+'/' + batchImg, outFile, radius)


    """
    # analysis(bitNumber, HPF_Th=0.0001, LPFDistStep=23, bias=0.0)
    countImg = 0
    with open('analysis_alotBPF_ideal.csv', 'w+') as file:
        for bitNumber in range(bitNumMin, bitNumMax + 1):
            for d0 in np.arange(0.8, 1.3, 0.1):
                for LPFDistStep in range(21, 26, 2):
                    imgName = 'deblurredImg_' \
                              + 'bitN=' + str(bitNumber) \
                              + '_dist=' + str(LPFDistStep) \
                              + '_d0=' + str(d0)
                    file.write(imgName+',')
                    for HPF_Th in np.arange(0.001, 0.00011, -0.0001):
                        file.write(str(analysis(bitNumber, HPF_Th, LPFDistStep, d0)))
                        file.write(',')
                        countImg = countImg + 1
                        print(countImg)

                    file.write('\n')
    """




if __name__ == '__main__':
    main()