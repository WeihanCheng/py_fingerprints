import numpy as np
from scipy.fftpack import fftn
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy.matlib
import cv2
import math
from mpl_toolkits.mplot3d import axes3d
import  csv
from scipy.signal import convolve2d as conv2
from skimage import color, data, restoration

paddingSize = 100


def genLoPaFilter(showImage=False):
    ### create low pass filter
    distStep = 23
    dist = np.array(np.linspace(0, 1, distStep))
    dist[0] = 0.000000001
    theta = (np.pi / 2) - np.arctan(1 / dist[:])
    ### 1-D low pass filter
    intensity = (np.cos(theta) * np.cos(theta)) / (1 / np.sin(np.pi / 2 - theta) ** 2)
    intensity = np.concatenate((intensity[-1:0:-1], intensity), axis=0)
    ### 2-D low pass filter
    intensity2D = np.matlib.repmat(intensity, distStep * 2 - 1, 1)
    intensity2D = intensity2D * intensity2D.T
    ### low pass filter normalization
    lowpassFilter = intensity2D / sum(sum(intensity2D))


    ### read image
    img = cv2.imread("without glass[1].png")
    img = img[:, :, 1]
    ### pad for fft
    img = cv2.copyMakeBorder(img, paddingSize, paddingSize, paddingSize, paddingSize, cv2.BORDER_CONSTANT, value=0)
    #plt.imshow(padImg, cmap='gray')
    #plt.show()

    #imgGray = padImg
    imgGray = img.astype(np.float64)

    rows = img.shape[0]
    cols = img.shape[1]

    ### padding for filter
    padLowpassFilter = np.zeros((rows, cols))
    range_x_begin = (rows-1)/2 -distStep+1
    range_x_end = (rows-1)/2 +distStep
    range_y_begin = (cols-1)/2 -distStep+1
    range_y_end = (cols-1)/2 +distStep
    padLowpassFilter[int(range_x_begin): int(range_x_end), int(range_y_begin): int(range_y_end)] = lowpassFilter

    ### 2-D convolution with 2-D low pass filter
    imgLowPass = cv2.filter2D(imgGray, -1, lowpassFilter)
    imgLowPass = imgLowPass.astype(np.float16)
    imgLowPass= imgLowPass.astype(np.float64)


    fft2DFilter = np.fft.fft2(padLowpassFilter)
    fft2DFilter = np.fft.fftshift(fft2DFilter)


    fftImg = np.fft.fft2(imgGray)
    fftImg = np.fft.fftshift(fftImg)

    ### generate HPF
    fftHPF = fft2DFilter
    for index in np.nditer(fftHPF, op_flags=['readwrite']):
        if abs(index[...]) > 0.0001:
            index[...] = index

        else:
            index[...] = 0.0001

    fftHPF = 1/fftHPF
    highpassFilter = np.fft.ifftshift(fftHPF)
    highpassFilter = np.fft.ifft2(highpassFilter)
    #highpassFilter = np.fft.ifftshift(highpassFilter)
    highpassFilter = np.real(highpassFilter)
    hpf = np.zeros((lowpassFilter.shape[0], lowpassFilter.shape[1]))
    hpf = highpassFilter[int(range_x_begin): int(range_x_end), int(range_y_begin): int(range_y_end)]


    """
    with open('highpassFilter_2.csv', 'w+') as file:
        writer = csv.writer(file, delimiter=',')
        for i in a:
            writer.writerow(i)
    """
    imgResto = cv2.filter2D(imgLowPass, -1, hpf)

    plt.subplot(121), plt.imshow(np.log(abs(fftHPF) + 1), cmap='gray'), plt.title('fftHPF_invLPF')
    plt.subplot(122), plt.imshow(np.log(abs(fft2DFilter) + 1), cmap='gray'), plt.title('fftLPF')
    plt.show()

    plt.subplot(131), plt.imshow(np.log(abs(fft2DFilter)+1), cmap='gray'), plt.title('fft2DFilter')
    plt.xticks([]), plt.yticks([])
    plt.subplot(132), plt.imshow(np.log(abs(fftImg)+1), cmap='gray'), plt.title('fftImg')
    plt.xticks([]), plt.yticks([])

    imgLowPassfft = fftImg * fft2DFilter
    plt.subplot(133), plt.imshow(np.log(abs(imgLowPassfft)+1), cmap='gray'), plt.title('imgLowPassfft')
    plt.xticks([]), plt.yticks([])
    plt.show()

    ### ifft low pass filtering image
    imgLowPassfft = np.fft.ifftshift(imgLowPassfft)
    imgLowPassfft = np.fft.ifft2(imgLowPassfft)
    imgLowPassfft = np.fft.ifftshift(imgLowPassfft)
    imgLPF = imgLowPassfft.real.astype(np.float16)

    fftImgLPF = np.fft.fft2(imgLPF)
    fftImgLPF = np.fft.fftshift(fftImgLPF)

    ### ifft restored image
    restoredImgLowPassfft = fftImg * fft2DFilter
    restoredImgLowPassfft = restoredImgLowPassfft * fftHPF
    restoredImgLowPassfft = np.fft.ifftshift(restoredImgLowPassfft)
    restoredImgLowPassfft = np.fft.ifft2(restoredImgLowPassfft)
    #restoredImgLowPassfft = np.fft.ifftshift(restoredImgLowPassfft)

    plt.subplot(131), plt.imshow(np.real(imgLowPassfft), cmap='gray'), plt.title('imgLowPassImg')
    plt.subplot(132), plt.imshow(np.real(restoredImgLowPassfft), cmap='gray'), plt.title('restoredImgLowPassImg')
    plt.subplot(133), plt.imshow(img, cmap='gray'), plt.title('Ori')
    plt.show()


    Y = np.arange(int(-(rows-1)/2), int((rows-1)/2 + 1), 1)
    X = np.arange(int(-(cols-1)/2), int((cols-1)/2 + 1), 1)

    X, Y = np.meshgrid(X, Y)
    ZLPF = abs(fft2DFilter)
    ZHPF = abs(fftHPF)
    """
    figLPF = plt.figure()
    axLPF = figLPF.gca(projection='3d')
    axLPF.plot_surface(X, Y, ZLPF, cmap=plt.get_cmap('rainbow')), plt.title('abs(fft2DFilter)')
    figHPF = plt.figure()
    axHPF = figHPF.gca(projection='3d')
    axHPF.plot_surface(X, Y, ZHPF, cmap=plt.get_cmap('rainbow')), plt.title('abs(fftHPF)')
    plt.show()
    """
    fftimgGray = np.fft.fft2(imgGray)
    fftimgGray = np.fft.fftshift(fftimgGray)
    Z1D = fftimgGray[int(-(rows - 1) / 2), :]
    plt.plot(10*np.log10(abs(Z1D))), plt.xlim(0, cols), plt.title('1D abs(fftimgGray)')
    plt.show()
    """
    # Restore Image using Richardson-Lucy algorithm
    imgLowPassNorm = imgLowPass / 255.0
    deconvolved_RL = restoration.richardson_lucy(imgLowPassNorm, lowpassFilter, iterations=20)
    plt.subplot(121), plt.imshow(imgLowPass, cmap='gray'), plt.title('blurred image')
    plt.subplot(122), plt.imshow(deconvolved_RL, cmap='gray'), plt.title('deblurred image')
    plt.show()
    """
    if showImage==True:
        plt.subplot(131), plt.imshow(img, cmap='gray'), plt.title('Original')
        plt.xticks([]), plt.yticks([])
        plt.subplot(132), plt.imshow(imgLowPass, cmap='gray'), plt.title('Low-Pass filtered image')
        plt.xticks([]), plt.yticks([])
        plt.subplot(133), plt.imshow(imgResto, cmap='gray'), plt.title('Restored image')
        plt.xticks([]), plt.yticks([])
        plt.show()

        #   1D fft
        fft1D = fftn(intensity)
        fft1D = np.fft.fftshift(fft1D)
        #   2D fft
        fft2D = np.fft.fft2(lowpassFilter)
        fft2D = np.fft.fftshift(fft2D)

        plt.subplot(121), plt.plot(np.abs(fft1D)), plt.xlim(0, fft1D.shape[0]), plt.title('1D fft image')
        plt.subplot(122), plt.imshow(abs(fft2D), cmap='gray'), plt.title('2D fft image')
        plt.show()

        X = np.arange(-(distStep - 1), distStep, 1)
        Y = np.arange(-(distStep - 1), distStep, 1)
        X, Y = np.meshgrid(X, Y)
        Z = lowpassFilter
        fig = plt.figure()
        ax = fig.gca(projection='3d')
        ax.plot_surface(X, Y, Z, cmap=plt.get_cmap('rainbow'))
        plt.show()

    return lowpassFilter

def genHiPaFilter(showImage=False):
    np.fft.fft(np.exp(2j * np.pi * np.arange(8) / 8))

    t = np.arange(256)
    sp = np.fft.fft(np.sin(t))
    freq = np.fft.fftfreq(t.shape[-1])
    plt.plot(freq, sp.real, freq, sp.imag)

    plt.show()