import numpy as np
from scipy.fftpack import fftn
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy.matlib
import cv2
import math
from mpl_toolkits.mplot3d import axes3d
import os
from skimage.measure import compare_ssim as ssim



img1= cv2.imread('real_2.png')
img2 = cv2.imread('real_3.png')

imgGray1 = cv2.cvtColor(img1, cv2.COLOR_RGB2GRAY)
imgGray2 = cv2.cvtColor(img2, cv2.COLOR_RGB2GRAY)

h, w = imgGray1.shape

imgGray2 = cv2.resize(imgGray2, (w, h), interpolation=cv2.INTER_LANCZOS4)

print(ssim(imgGray1, imgGray2))









