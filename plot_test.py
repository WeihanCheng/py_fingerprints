import numpy as np
from scipy.fftpack import fftn
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy.matlib
import cv2
import math
from mpl_toolkits.mplot3d import axes3d



a = np.zeros(1, dtype=np.complex)
a.real = 379.86
a.imag = -1150.0268
print(a)

a_float = a.real.astype(float)
a_float = a_float*(10**16)
a_int = int(a_float)
a_float = float(a_int)
a_float = a_float/(10**16)

a_float_im = a.imag.astype(float)
a_float_im = a_float_im*(10**16)
a_int_im = int(a_float_im)
a_float_im = float(a_int_im)
a_float_im = a_float_im/(10**16)

print(a_float)
print(a_float_im)
print(type(a_float))
print(type(a_float_im))


