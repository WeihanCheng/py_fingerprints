import numpy as np
from scipy.fftpack import fftn
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy.matlib
import cv2
import math
from mpl_toolkits.mplot3d import axes3d
import os


def main():

    fileName = ['DB1_106']
    ROIPath = 'fingerprintROI/'

    widthRange = 20

    blockSize = 25

    for file in fileName:
        for batchImg in os.listdir(file):

            batchName = batchImg.split('.')[0]
            if not os.path.exists(ROIPath + batchName):
                os.makedirs(ROIPath + batchName)

            userID = ROIPath + batchName + '/A'
            if not os.path.exists(userID):
                os.makedirs(userID)

            fingerID = userID + '/1'
            if not os.path.exists(fingerID):
                os.makedirs(fingerID)

            enrollfile = fingerID + '/enroll'
            if not os.path.exists(enrollfile):
                os.makedirs(enrollfile)
            enrollfileSt = enrollfile + '/st'
            if not os.path.exists(enrollfileSt):
                os.makedirs(enrollfileSt)

            verifyfile = fingerID + '/verify'
            if not os.path.exists(verifyfile):
                os.makedirs(verifyfile)
            verifyfileSt = verifyfile + '/st'
            if not os.path.exists(verifyfileSt):
                os.makedirs(verifyfileSt)


            img = cv2.imread(file + '/' + batchImg)
            imgGray = img[:, :, 1]
            h, w = imgGray.shape
            left = w
            right = 0
            top = h
            bottom = 0

            for i in range(0, h, blockSize):
                for j in range(widthRange, w - widthRange, blockSize):
                    block = imgGray[i: i+blockSize, j: j+blockSize]
                    blockVar = np.var(block)
                    if blockVar > 180.0:
                        if i < top: top = i
                        if j < left: left = j
                        if i > bottom: bottom = i
                        if j > right: right = j

            #fpROI =cv2.rectangle(img, (left, top), (right, bottom), (0, 0, 0), 3)
            #plt.imshow(fpROI, cmap='gray'), plt.title('fpROI'), plt.show()

            heightStep = int(50)
            widthStep = int(30)
            count = 0
            for i in range(top, top + heightStep*4, heightStep):
                for j in range(left, left + widthStep*4, widthStep):
                    #fpCrop = cv2.rectangle(fpROI, (j, i), (j + 180 - 1, i + 90 - 1), (0, 0, 0), 1)
                    count += 1
                    plt.imsave(enrollfileSt + '/' + str(count) + '.png', imgGray[i: i + 90, j: j + 180], cmap=cm.gray)
            count = 0
            for i in range(top, top + heightStep*4, heightStep):
                for j in range(left + 10, left + widthStep*4 + 10, widthStep):
                    #fpCrop = cv2.rectangle(fpROI, (j, i), (j + 180 - 1, i + 90 - 1), (255, 0, 0), 1)
                    count += 1
                    plt.imsave(verifyfileSt + '/' + str(count) + '.png', imgGray[i: i + 90, j: j + 180], cmap=cm.gray)


            #plt.imshow(fpCrop, cmap='gray'), plt.title('fpROI'), plt.show()






if __name__ == '__main__':
    main()




























