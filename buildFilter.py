import numpy as np
from scipy.fftpack import fftn
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy.matlib
import cv2
import math
from mpl_toolkits.mplot3d import axes3d



def LPF(distStep, d0=1.0):
    ### create low pass filter
    dist = np.array(np.linspace(0, 1, distStep))
    dist[0] = 0.000000001
    theta = (np.pi / 2) - np.arctan(d0 / dist[:])
    ### 1-D low pass filter
    intensity = (np.cos(theta) * np.cos(theta)) / ((1 / np.sin(np.pi / 2 - theta) ** 2)*(d0**2))
    intensity = np.concatenate((intensity[-1:0:-1], intensity), axis=0)
    ### 2-D low pass filter
    intensity2D = np.matlib.repmat(intensity, distStep * 2 - 1, 1)
    intensity2D = intensity2D * intensity2D.T
    ### low pass filter normalization
    lowpassFilter = intensity2D / sum(sum(intensity2D))

    return lowpassFilter

def fftHPF(rows, cols, distStep=23, th = 0.0001):

    lowPassFilter = LPF(distStep)

    ### pad LPF for fft computation
    padLPFrows = np.int((rows - lowPassFilter.shape[0]) / 2)
    padLPFcols = np.int((cols - lowPassFilter.shape[1]) / 2)
    padLPF = cv2.copyMakeBorder(lowPassFilter, padLPFrows, padLPFrows, padLPFcols, padLPFcols, cv2.BORDER_CONSTANT, value=0)

    fftpadLPF = np.fft.fft2(padLPF)
    fftpadLPF = np.fft.fftshift(fftpadLPF)
    #Z1 = fftpadLPF[int(-(rows - 1) / 2), :]
    #plt.plot(20 * np.log10(abs(Z1))), plt.xlim(0, cols), plt.title('1D fftpadLPF')

    fftHPF = fftpadLPF

    for index in np.nditer(fftHPF, op_flags=['readwrite']):
        if abs(index[...]) > th:
            index[...] = index

        else:
            index[...] = th

    fftHPF = 1 / fftHPF

    #Z2 = fftHPF[int(-(rows - 1) / 2), :]
    #plt.plot(-20*np.log10(abs(Z2))), plt.xlim(0, cols), plt.title('1D fftHPF')
    #plt.show()
    #plt.savefig('th='+str(th)+'.png')
    #plt.close()

    return fftHPF

def BPF(imgRows, imgCols, sigmaGLPF=None, sigmaGHPF=None):

    filterSize = 45

    GLPF = np.zeros((imgRows, imgCols))
    GHPF = np.zeros((imgRows, imgCols))

    ### build gaussian low pass filter
    for i in range(int(-(imgRows - 1) / 2), int((imgRows - 1) / 2) + 1):
        for j in range(int(-(imgCols - 1) / 2), int((imgCols - 1) / 2) + 1):
            GLPF[int(i + (imgRows - 1) / 2), int(j + (imgCols - 1) / 2)] = (1./(2*np.pi*sigmaGLPF*sigmaGLPF)) * math.exp(-(i*i+j*j)/(2*sigmaGLPF*sigmaGLPF))
            GHPF[int(i + (imgRows - 1) / 2), int(j + (imgCols - 1) / 2)] = (1./(2*np.pi*sigmaGHPF*sigmaGHPF)) * math.exp(-(i*i+j*j)/(2*sigmaGHPF*sigmaGHPF))

    fftGLPF = np.fft.fft2(GLPF)
    fftGLPF = np.fft.fftshift(fftGLPF)
    fftGHPF = np.fft.fft2(GHPF)
    fftGHPF = np.fft.fftshift(fftGHPF)


    plt.subplot(131), plt.imshow(GLPF, cmap='gray'), plt.title('low pass filter')
    plt.subplot(132), plt.imshow(GHPF, cmap='gray'), plt.title('high pass filter')
    #plt.subplot(133), plt.imshow(GBPF, cmap='gray'), plt.title('band pass filter')
    plt.show()







